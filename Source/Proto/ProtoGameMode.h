// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "ProtoGameMode.generated.h"

UCLASS(minimalapi)
class AProtoGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AProtoGameMode();
};




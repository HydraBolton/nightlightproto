// Fill out your copyright notice in the Description page of Project Settings.

#include "Proto.h"
#include "ProtoCharacter.h"
#include "Mannequin.h"




// Sets default values
AMannequin::AMannequin()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

AMannequin::AMannequin(const class FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	speed = 20;
	health = 1000;
	attackDamage = 10;
	attackTime = 1.5f;
	TimeSinceLastAttack = 0;

	attackRange = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("attackRange"));
	attackRange->AttachTo(RootComponent);
}

// Called when the game starts or when spawned
void AMannequin::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMannequin::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	AProtoCharacter *character = Cast<AProtoCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (!character) return;

	FVector playerLoc = character->GetActorLocation() - GetActorLocation();
	playerLoc.Normalize();

	

	FRotator playerLocRotation = playerLoc.Rotation();
	playerLocRotation.Pitch = 0;
	RootComponent->SetWorldRotation(playerLocRotation);
}

// Called to bind functionality to input
void AMannequin::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

